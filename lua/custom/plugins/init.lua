-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- set relative line numbers
vim.wo.relativenumber = true
-- See the kickstart.nvim README for more information
return {}
